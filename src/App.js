import React from 'react';
import Header from './compoments/Header';
import Meme from './compoments/Meme';

export default function App() {
  return (
    <div>
      <Header />
      <Meme />
    </div>
  );
}

